import 'ol/ol.css'

import { FeatureCollection } from 'geojson'
import { Graticule } from 'ol'
import { defaults as defaultControls, ScaleLine } from 'ol/control'
import MousePosition from 'ol/control/MousePosition'
import GeoJSON from 'ol/format/GeoJSON'
import { Vector as VectorLayer } from 'ol/layer'
import BaseLayer from 'ol/layer/Base'
import ImageLayer from 'ol/layer/Image'
import Map from 'ol/Map'
import { Vector as VectorSource } from 'ol/source'
import ImageStatic from 'ol/source/ImageStatic'
import { Fill, Stroke, Text } from 'ol/style'
import View from 'ol/View'

import { GeoChroniclesOfElyria } from './GeoCoE'
import { IMyDomain, KINGDOM } from './interfaces'
import { styleFunction } from './styleFunction'

const coe = new GeoChroniclesOfElyria()


const newLayer = (kingdom: IMyDomain.IMyDomainKingdom, minResolution: number, maxResolution: number, features: IMyDomain[]) => {
    const featureCollection: FeatureCollection = {
      bbox: kingdom.bbox,
      features,
      type: 'FeatureCollection',
    }

    const vectorSource = new VectorSource({
        features: (new GeoJSON()).readFeatures(featureCollection, {dataProjection: GeoChroniclesOfElyria.epsg4326projection}),
    })
    vectorSource.getFeatures().forEach((feature) => feature.getGeometry().transform(GeoChroniclesOfElyria.epsg4326projection, coe.projection))

    return new VectorLayer({
        maxResolution,
        minResolution,
        source: vectorSource,
        style: styleFunction,
    })
}


(async () => {
    const kingdom = (await coe.getKingdom(KINGDOM.Demalion))
    const duchies = kingdom.Subdomains
    const counties = duchies.map((sd) => sd.Subdomains).reduce((flat, item) => flat.concat(item), [])
    const settlements = counties.map((sd) => sd.Subdomains).reduce((flat, item) => flat.concat(item), [])

    const domainLayers = [
        newLayer(kingdom, 0, 1 / 4, settlements),  // Settlements
        newLayer(kingdom, 0, 1 / 2, counties),  // Counties
        newLayer(kingdom, 1 / 4, 2, duchies),  // Duchies
        newLayer(kingdom, 1 / 2, 99, [kingdom]),
    ]

    console.info(kingdom)
    console.info(settlements.find((sett) => sett.properties.TemporaryName.includes('Baien Den')).geometry)

    const layers = [
        // new ImageLayer({
        //     source: new ImageStatic({
        //         imageExtent: [-740, -1360, 860, 1640],
        //         projection: projectionParcels,
        //         url: './Selene-8.png',
        //     }),
        // }),
        new ImageLayer({
            source: new ImageStatic({
                imageExtent: [-740, -1360, 860, 1640],
                projection: coe.projection,
                url: './Selene-8.png',
            }),
        }),
        new ImageLayer({
            source: new ImageStatic({
                imageExtent: [8 * 100 - 1440, 640, 660, 1640],
                projection: coe.projection,
                url: './Demalion-4.png',
            }),
        }),

        new Graticule({
            intervals: [0.0625 * 1000, 0.0625 * 100, 0.0625 * 10, 0.0625],
            latLabelFormatter: (lat) => `${lat * 16}  `,
            //// wait until fix https://github.com/openlayers/openlayers/issues/10092
            latLabelStyle: new Text({
                fill: new Fill({
                    color: 'rgba(0,0,0,1)',
                }),
                font: '12px Calibri,sans-serif',
                stroke: new Stroke({
                    color: 'rgba(255,255,255,1)',
                    width: 3,
                }),
                textAlign: 'start',
                textBaseline: 'bottom',
            }),
            lonLabelFormatter: (lon) => `${lon * 16}  `,
            lonLabelStyle: new Text({
                fill: new Fill({
                    color: 'rgba(0,0,0,1)',
                }),
                font: '12px Calibri,sans-serif',
                offsetX: 4,
                stroke: new Stroke({
                    color: 'rgba(255,255,255,1)',
                    width: 3,
                }),
                textAlign: 'end',
                textBaseline: 'bottom',
            }),
            maxLines: 250,
            showLabels: true,
            strokeStyle: new Stroke({
                color: 'rgba(255,120,0,0.6)',
                lineDash: [2, 4],
                width: 1,
            }),
        }),

        ...domainLayers,
    ] as BaseLayer[]

    // https://openlayers.org/en/latest/examples/mouse-position.html?q=mouse
    const mousePositionControlParcels = new MousePosition({
        className: 'custom-mouse-position-parcels',
        coordinateFormat: ([x, y]) => `Parcels: ${Math.floor(x)}, ${Math.floor(y)}`,
        projection: coe.projection,
        target: document.getElementById('mouse-position-parcels'),
        undefinedHTML: '&nbsp;',
    })
    const mousePositionControlLatLong = new MousePosition({
        className: 'custom-mouse-position-latlong',
        coordinateFormat: ([x, y]) => `Lat.Long.: ${Math.floor(x * 16) / 16}, ${Math.floor(y * 16) / 16}`,
        projection: GeoChroniclesOfElyria.epsg4326projection,
        target: document.getElementById('mouse-position-latlong'),
        undefinedHTML: '&nbsp;',
    })

    // tslint:disable-next-line: no-dead-store
    const map = new Map({
        controls: defaultControls().extend([
            new ScaleLine({
            }),
            mousePositionControlParcels,
            mousePositionControlLatLong,
        ]),
        layers,
        target: 'map',
        view: new View({
            center: [0, 0],
            extent: coe.extent,
            projection: coe.projection,
            zoom: 0,
        }),
    })

    console.info('Done!')
})()
    .catch((err) => console.error(err))

