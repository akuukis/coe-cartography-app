import intersect from '@turf/intersect'
import { lstatSync, mkdirSync, readFileSync, writeFileSync } from 'fs'
import { Polygon as GeoJsonPolygon } from 'geojson'
import fetch from 'node-fetch'
import GeoJSON from 'ol/format/GeoJSON'
import Circle from 'ol/geom/Circle'
import Polygon, { fromCircle } from 'ol/geom/Polygon'
import { join, sep } from 'path'

import { GeoChroniclesOfElyria } from './GeoCoE'
import { IDomainId, IMyDomain, ParentDomain } from './interfaces'
import { DOMAIN_TYPE, IDomain, IGeo, PARCELS_PER_LATLNG } from './vendor'

const TMP_DIR = 'tmp'
const DOMAINS_DIR = join('tmp', 'domains')


const dataUrl = (domain: DOMAIN_TYPE, {id, serverId}: IDomainId) => `https://chroniclesofelyria.com/domains/api/domain/${serverId}/${domain}?parentIds%5B0%5D=${id}`
const geoUrl = (domain: DOMAIN_TYPE, {id, serverId}: IDomainId) => `https://chroniclesofelyria.com/domains/api/domain/geo/${serverId}/${domain}?parentIds%5B0%5D=${id}`

const mkdirp = (dir: string) => {
    dir.split(sep).forEach((_, i, dirs) => {
        try {
            mkdirSync(join(...dirs.slice(0, i + 1)))
        } catch(err) {
            // Do nothing.
        }
    })
}

export interface IPair<T extends DOMAIN_TYPE> {
    data: [ParentDomain<T>, ...Array<IDomain[T]>]
    geo: IGeo
}
export const cacheDomain = async <T extends DOMAIN_TYPE>(domain: T, {id, serverId}: IDomainId): Promise<IPair<T>> => {
    const domainDir = join(TMP_DIR, `server-${serverId}`, `domain-${String(domain)}`)
    mkdirp(domainDir)
    const filenameData = join(domainDir, `${id}-data.json`)
    const filenameGeo = join(domainDir, `${id}-geo.json`)
    try {
        // Skip downloading again.
        lstatSync(filenameData)
        lstatSync(filenameGeo)

        return {
            data: JSON.parse(readFileSync(filenameData).toString('utf-8')) as [ParentDomain<T>, ...Array<IDomain[T]>],
            geo: JSON.parse(readFileSync(filenameGeo).toString('utf-8')) as IGeo,
        }
    } catch(err) {
        // console.log(`${serverId}-${String(domain)}-${id}`)
        const data = await (await fetch(dataUrl(domain, {id, serverId}))).text()
        writeFileSync(join(filenameData), data)
        const geo = await (await fetch(geoUrl(domain, {id, serverId}))).text()
        writeFileSync(filenameGeo, geo)

        return {
            data: JSON.parse(data) as [ParentDomain<T>, ...Array<IDomain[T]>],
            geo: JSON.parse(geo) as IGeo,
        }
    }
}

export const saveAsLameCsv = (filename: string, data: IDomain.IDomainAny[]) => {
    const SEPERATOR = ';'
    const keys = Object.keys(data[0]) as Array<keyof IDomain.IDomainAny>
    const lines: string[] = []
    for(const datum of data) lines.push(keys.map((key) => datum[key] === undefined ? '' : JSON.stringify(datum[key])).join(SEPERATOR))

    const content = `${keys.join(SEPERATOR)}\n${lines.join('\n')}`
    writeFileSync(join(TMP_DIR, `${filename}.csv`), content)
}

// tslint:disable: max-line-length
export const cacheKingdom = async ({id, serverId}: IDomainId) => {
    const coe = new GeoChroniclesOfElyria()
    mkdirp(TMP_DIR)
    const filename = join(TMP_DIR, `server-${serverId}`, `kingdom-${id}.json`)
    try {
        // Skip downloading again.
        lstatSync(filename)

        return JSON.parse(readFileSync(filename).toString('utf-8')) as IMyDomain.IMyDomainKingdom
    } catch(err) {

        const worldCache = (await cacheDomain(DOMAIN_TYPE.KINGDOM, {id, serverId}))
        const kingdomData = worldCache.data
            .find((cache) => cache.Id === id)
        const kingdomFeature = worldCache.geo.Features.features
            .find((feature) => feature.properties.id === id)

        const duchyCaches = (await cacheDomain(DOMAIN_TYPE.DUCHY, {id, serverId}))
        const [_, ...duchyDatas] = duchyCaches.data  // tslint:disable-line: naming-convention no-dead-store no-shadowed-variable
        const duchies = await Promise.all(duchyDatas.map<Promise<IMyDomain.IMyDomainDuchy>>(async (duchyData) => {

            const countyCaches = (await cacheDomain(DOMAIN_TYPE.COUNTY, {id: duchyData.Id, serverId}))
            const [_, ...countyDatas] = countyCaches.data  // tslint:disable-line: naming-convention no-dead-store no-shadowed-variable
            const duchyGeo = countyCaches.geo.ParentsBorder.features[0]

            return {
                bbox: duchyGeo.bbox,
                geometry: duchyGeo.geometry,
                properties: duchyData,
                Subdomains: await Promise.all(countyDatas.map<Promise<IMyDomain.IMyDomainCounty>>(async (countyData) => {

                    const settlementCaches = (await cacheDomain(DOMAIN_TYPE.SETTLEMENT, {id: countyData.Id, serverId}))
                    const [_, ...settlementDatas] = settlementCaches.data  // tslint:disable-line: naming-convention no-dead-store no-shadowed-variable
                    const countyGeo = settlementCaches.geo.ParentsBorder.features[0]

                    return {
                        bbox: countyGeo.bbox,
                        geometry: countyGeo.geometry,
                        properties: countyData,
                        Subdomains: (settlementDatas as unknown as IDomain.IDomainSettlement[]).map<IMyDomain.IMyDomainSettlement>((settlementData) => {

                            let k = 1
                            let geometry: GeoJsonPolygon
                            const minParcels = Number(settlementData.ParcelCount.replace(/\,|\s/, '').split('-')[0])
                            const radius = Math.sqrt(minParcels / Math.PI) / PARCELS_PER_LATLNG
                            while(true) {
                                const circle = new Circle([...settlementData.Location].reverse(), radius * k)
                                const polygon = fromCircle(circle)
                                const polyCircle = (new GeoJSON()).writeGeometryObject(polygon) as GeoJsonPolygon
                                geometry = intersect(countyGeo.geometry, polyCircle).geometry as GeoJsonPolygon
                                const intersectionPolygon = new Polygon(geometry.coordinates)
                                const intersectionAreaInParcels = intersectionPolygon.getArea() * PARCELS_PER_LATLNG * PARCELS_PER_LATLNG
                                if(intersectionAreaInParcels / minParcels < 0.99) {
                                    k = k + 0.01
                                } else {
                                    break
                                }
                            }

                            return {
                                bbox: countyGeo.bbox,
                                geometry,
                                PanCoordinates: settlementData.Location,
                                properties: settlementData,
                                Subdomains: null,
                                type: 'Feature',
                            }
                        }),
                        type: 'Feature',
                    }
                })),
                type: 'Feature',
            }
        }))
        const kingdom: IMyDomain.IMyDomainKingdom = {
            bbox: kingdomFeature.bbox,
            geometry: kingdomFeature.geometry,
            properties: kingdomData,
            Subdomains: duchies,
            type: 'Feature',
        }
        writeFileSync(filename, JSON.stringify(kingdom))

        return kingdom
    }

}
