// tslint:disable:no-implicit-dependencies
import { Extent } from 'ol/extent'
import { addCoordinateTransforms, get } from 'ol/proj'
import { register } from 'ol/proj/proj4'
import Projection from 'ol/proj/Projection'
import proj4 from 'proj4/lib'

import { IMyDomain, Kingdom } from './interfaces'
import {
    DOMAIN_TYPE,
    fromLatLngToParcels,
    fromParcelsToLatLng,
    METERS_PER_PARCEL,
    PARCELS_PER_LATLNG,
    WORLD_EXTENT,
} from './vendor'


export const getPoint = (center: [number, number], i: number, radius: number) => {
    const radians = i * 10 * Math.PI / 180
    const x = center[0] + radius * Math.cos(radians)
    const y = center[1] + radius * Math.sin(radians)

    return [x, y]
}

// tslint:disable-next-line: min-class-cohesion
export class GeoChroniclesOfElyria {
    public static epsg4326projection: Projection

    public static init() {
        // https://mgimond.github.io/Spatial/coordinate-systems-in-r.html
        // https://proj.org/operations/projections/cea.html
        // https://openlayers.org/en/latest/examples/reprojection-image.html?q=projection
        // https://openlayers.org/en/latest/examples/wms-image-custom-proj.html?q=projection
        // tslint:disable-next-line: no-unsafe-any
        proj4.defs('CoE:parcels', '+proj=cea +k_0=64')
        register(proj4)
        GeoChroniclesOfElyria.epsg4326projection = get('EPSG:4326')
    }
    public readonly extent: Extent
    public projection: Projection
    public readonly size: [number, number]

    // tslint:disable-next-line: no-map-without-usage
    public constructor(extent: Extent = WORLD_EXTENT.map((latlong) => latlong * PARCELS_PER_LATLNG)) {
        this.extent = extent
        GeoChroniclesOfElyria.init()

        this.projection = new Projection({
            code: 'CoE:parcels',
            extent,
            getPointResolution: (resolution) => resolution * METERS_PER_PARCEL,
            worldExtent: WORLD_EXTENT.map((latlong) => latlong * PARCELS_PER_LATLNG),
        })
        addCoordinateTransforms(
            GeoChroniclesOfElyria.epsg4326projection,
            this.projection,
            fromLatLngToParcels,
            fromParcelsToLatLng,
        )

    }

    public async getKingdom<T extends DOMAIN_TYPE>(kingdom: Kingdom) {
        return (await (await fetch(`sbs/server-${kingdom.serverId}/kingdom-${kingdom.id}.json`)).json()) as IMyDomain.IMyDomainKingdom
    }

}
