import { cacheDomain, cacheKingdom, saveAsLameCsv } from './cache'
import { KINGDOM } from './interfaces'
import { DOMAIN_TYPE, IDomain } from './vendor'


const myKingdom = KINGDOM.Demalion

;
(async () => {

    const duchies: IDomain.IDomainDuchy[] = []
    {
        const {data} = await cacheDomain(DOMAIN_TYPE.DUCHY, myKingdom)
        duchies.push(...data.slice(1) as IDomain.IDomainDuchy[])
    }
    saveAsLameCsv('duchies', duchies)

    const counties: IDomain.IDomainCounty[] = []
    for(const duchy of duchies) {

        // console.info('#  ', duchy.FinalName || duchy.TemporaryName, duchy.ParcelCount)
        const {data} = await cacheDomain(DOMAIN_TYPE.COUNTY, {id: duchy.Id, serverId: myKingdom.serverId})
        if(data === null) continue
        // console.info(data.slice(1).map((datum) => `    ${datum.FinalName || datum.TemporaryName} (${datum.ParcelCount})`).join('\n'))
        counties.push(...data.slice(1) as IDomain.IDomainCounty[])
    }
    saveAsLameCsv('counties', counties)

    const settlements: IDomain.IDomainSettlement[] = []
    for(const county of counties) {
        // console.info('###', county.TemporaryName, county.ParcelCount)
        const {data} = await cacheDomain(DOMAIN_TYPE.SETTLEMENT, {id: county.Id, serverId: myKingdom.serverId})
        settlements.push(...data.slice(1) as IDomain.IDomainSettlement[])
        // console.info(data.slice(1).map((datum) => `    ${datum.FinalName || datum.TemporaryName} (${datum.ParcelCount})`).join('\n'))
    }
    saveAsLameCsv('settlements', settlements)

    await cacheKingdom(myKingdom)
})()
    .catch((err) => console.error(err))
