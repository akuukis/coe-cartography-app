import { BBox, Feature, FeatureCollection, Polygon } from 'geojson'

import { DOMAIN_TYPE, IDomain, IGeoEBProperties, IGeoSettlement, SERVER_ID } from './vendor'


export interface IDomainId {
    id: number,
    serverId: SERVER_ID
}

export type ParentDomain<T extends DOMAIN_TYPE = DOMAIN_TYPE> =
    IDomain[T] extends IDomain.IDomainSettlement ? DOMAIN_TYPE.COUNTY :
    IDomain[T] extends IDomain.IDomainCounty ? DOMAIN_TYPE.DUCHY :
    IDomain[T] extends IDomain.IDomainDuchy ? DOMAIN_TYPE.KINGDOM :
    never
export type SubDomain<T extends DOMAIN_TYPE = DOMAIN_TYPE> =
    IDomain[T] extends IDomain.IDomainKingdom ? DOMAIN_TYPE.DUCHY :
    IDomain[T] extends IDomain.IDomainDuchy ? DOMAIN_TYPE.COUNTY :
    IDomain[T] extends IDomain.IDomainCounty ? DOMAIN_TYPE.SETTLEMENT :
    never

export interface IMerged<TDomain extends DOMAIN_TYPE = DOMAIN_TYPE> {
    Bounds: [[number, number], [number, number]],
    ExternalBorders: FeatureCollection<Polygon, IGeoEBProperties>,
    Features: FeatureCollection<Polygon, SubDomain<TDomain>>,  // That's my children.
    PanCoordinates: [number, number],
    ParentsBorder: FeatureCollection<Polygon, TDomain>,  // That's me. Always only one feature.
    Settlements: IGeoSettlement[],
}


export type IMyDomain =
    | IMyDomain.IMyDomainKingdom
    | IMyDomain.IMyDomainDuchy
    | IMyDomain.IMyDomainCounty
    | IMyDomain.IMyDomainSettlement

export namespace IMyDomain {

    export interface IMyDomainBase<T extends DOMAIN_TYPE = DOMAIN_TYPE> extends Feature<Polygon, IDomain[T]> {
        bbox: BBox,
        Subdomains: Array<IMyDomainBase<SubDomain<T>>>,
    }

    export interface IMyDomainSettlement extends IMyDomainBase<DOMAIN_TYPE.SETTLEMENT> {}
    export interface IMyDomainCounty extends IMyDomainBase<DOMAIN_TYPE.COUNTY> {}
    export interface IMyDomainDuchy extends IMyDomainBase<DOMAIN_TYPE.DUCHY> {}
    export interface IMyDomainKingdom extends IMyDomainBase<DOMAIN_TYPE.KINGDOM> {}
}

export const KINGDOM = {
    // tslint:disable: object-literal-sort-keys
    Riftwood    : {serverId: SERVER_ID.AGNELICA, id: 1},
    Tyria       : {serverId: SERVER_ID.AGNELICA, id: 1083},
    Blackheart  : {serverId: SERVER_ID.AGNELICA, id: 2460},
    Valyria     : {serverId: SERVER_ID.AGNELICA, id: 3823},
    Aranor      : {serverId: SERVER_ID.AGNELICA, id: 5225},
    Ashland     : {serverId: SERVER_ID.AGNELICA, id: 6665},

    Kairos      : {serverId: SERVER_ID.LUNA, id: 10249},
    Alésia      : {serverId: SERVER_ID.LUNA, id: 11493},
    Bordweall   : {serverId: SERVER_ID.LUNA, id: 13126},
    Fortuna     : {serverId: SERVER_ID.LUNA, id: 14640},
    Vornair     : {serverId: SERVER_ID.LUNA, id: 27531},

    Tryggr      : {serverId: SERVER_ID.SELENE, id: 17329},
    Nirath      : {serverId: SERVER_ID.SELENE, id: 18608},
    Al_khezam   : {serverId: SERVER_ID.SELENE, id: 21337},
    Arkadia     : {serverId: SERVER_ID.SELENE, id: 22697},
    Demalion    : {serverId: SERVER_ID.SELENE, id: 27532},

    Aequitas    : {serverId: SERVER_ID.OCEANUS, id: 23910},
    Tylsia      : {serverId: SERVER_ID.OCEANUS, id: 24649},
    Caprakan    : {serverId: SERVER_ID.OCEANUS, id: 25431},
    Lor_Voskara : {serverId: SERVER_ID.OCEANUS, id: 26443},
    // tslint:enable: object-literal-sort-keys
} as const
export type Kingdom = typeof KINGDOM[keyof typeof KINGDOM]
