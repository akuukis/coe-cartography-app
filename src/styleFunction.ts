import Feature, { FeatureLike } from 'ol/Feature'
import { Fill, Stroke, Style, Text } from 'ol/style'

import { IMyDomain } from './interfaces'
import { DOMAIN_TYPE } from './vendor'


// http://stackoverflow.com/questions/14484787/wrap-text-in-javascript
const stringDivider = (str: string, width: number, spaceReplacer: string): string => {
    let p = str.length
    while (p > 0 && (str[p] !== ' ' && str[p] !== '-')) p -= 1
    if (p === 0) return str

    const left = str.substring(p, p + 1) === '-' ? str.substring(0, p + 1) : str.substring(0, p)
    const right = str.substring(p + 1)

    return `${left}${spaceReplacer}${stringDivider(right, width, spaceReplacer)}`
}

const CACHED_LABEL_SIZE = Symbol('CACHED_LABEL_SIZE')
const PARCELS_IN_KM2 = 1000 * 1000 / (64 * 64)

const getMyDomainLabelStyle = (feature: Feature, resolution: number) => {
    const props = feature.getProperties() as IMyDomain.IMyDomainKingdom['properties']

    if(!props[CACHED_LABEL_SIZE]) {
        const parcels = Number(props.ParcelCount.replace(/\,|\s/, '').split('-')[0]) * (props.DomainType > 2 ? PARCELS_IN_KM2 : 1)
        props[CACHED_LABEL_SIZE] = Math.sqrt(parcels) / 64
        feature.setProperties(props, true)
    }

    return new Text({
        fill: new Fill({color: '#FFFA'}),
        font: `'Passion One', cursive`,
        overflow: true,
        scale: props[CACHED_LABEL_SIZE] as number / resolution,
        textAlign: 'middle',
    })
}

const styleFuctions: Record<DOMAIN_TYPE, (p0: FeatureLike, p1: number) => Style> = {
    // tslint:disable-next-line: no-identical-functions - TODO: Specialized feature classes?
    [DOMAIN_TYPE.KINGDOM]: (feature, resolution) => {
        return new Style({
            stroke: new Stroke({
                color: 'rgba(172, 157, 255, 1)',
                width: 4,
            }),
            text: getMyDomainLabelStyle(feature as Feature, resolution),
        })
    },
    // tslint:disable-next-line: no-identical-functions - TODO: Specialized feature classes?
    [DOMAIN_TYPE.DUCHY]: (feature, resolution) => {
        return new Style({
            stroke: new Stroke({
                color: 'rgba(255, 157, 240, 1)',
                width: 3,
            }),
            text: getMyDomainLabelStyle(feature as Feature, resolution),
        })
    },
    // tslint:disable-next-line: no-identical-functions - TODO: Specialized feature classes?
    [DOMAIN_TYPE.COUNTY]: (feature, resolution) => {
        return new Style({
            stroke: new Stroke({
                color: 'rgba(240, 255, 157, 1)',
                width: 2,
            }),
            text: getMyDomainLabelStyle(feature as Feature, resolution),
        })
    },
    [DOMAIN_TYPE.SETTLEMENT]: (feature, resolution) => {
        return new Style({
            fill: new Fill({
                color: 'rgba(157, 255, 172, 0.2)',
            }),
            stroke: new Stroke({
                color: 'rgba(157, 255, 172, 1)',
                width: 1,
            }),
            text: getMyDomainLabelStyle(feature as Feature, resolution),
        })
    },
}


export const styleFunction = (feature: FeatureLike, resolution: number) => {
    const props = feature.getProperties() as IMyDomain.IMyDomainSettlement['properties']
    const style = styleFuctions[props.DomainType](feature, resolution)
    style.getText().setText(stringDivider(props.TemporaryName, 4, '\n'))

    return style
}
