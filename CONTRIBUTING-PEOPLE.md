TODO




## UI brainstorm

I like the combination of these
- *www.openstreetmap.org* - top-right menu, especially "layers" button. Will need few of those to control
    - show/hide elements,
    - based on what properties to highlight elements, 
    - manage user annotations & metadata,
    - select background,
    - "share map",
    - toolbox - calculate distance, etc.
    - and other future features
- *wego.here.com* - top-left menu icon + left drawer. Feels much better to put external links and credits there than topbar as in OSM
- *any online map* - top-left search menu, and unfolding details below in once something is selected. Many variations based on what's selected
- *any online map* - buttom-right corner "small print" and scale in both parcels and meters
