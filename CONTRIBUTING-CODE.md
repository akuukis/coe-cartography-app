TODO




## Get started locally locally.

> FYI: I recommend using VSCode IDE because I've already configured it (open workspace within `./vscode` folder).

1. Install NodeJS
2. Install Yarn
3. `yarn install`
4. `yarn build`
5. Open `./dist/index.html` in your favorite browser.
6. Run `yarn start` and start editing files.

Read `package.json` scripts for everything you most likely need (I bet you want).


## DSS data

### Coordinate systems

Here's example of DSS data.
```json
{
    "Id": 20403,
    "LatLng": [68.8125, -29.0],
    "Name": "Cragleffit Point",
}
{
    "Id": 20404,
    "LatLng": [68.125, -29.1875],
    "Name": "The Baker Dike",
}
```

From DSS website we see that "Cragleffit Point" is 3 parcels to east and 11 north. Therefore,
- format is `[latitude, longitude]`
- latlong:parcel ratio is 1:16 (or 0.625:1).
- 0:0 corner is bottom-left.
- as we know, latitude is from -90 to 90, and longitude is -180 to +180. But the domains goes well above latitude 90. So I assume -180 to 180 latitude.
