# Chronicles of Elyria Cartography App

Cartography App is a cross-server tool to create and share custom maps of Chronicles of Elyria.

Maps made with Cartography App:
- [Selene] Demalion kingdom: http://coe-maps-dev.s3-website-eu-west-1.amazonaws.com/
- *... new public maps will be added here ...*


Do you want to create a map? Join at Discord [here](https://discord.gg/xsm7TeV) and let's chat!

P.S. Forum post: https://chroniclesofelyria.com/forum/topic/33607/Cartography-App-for-Chronicles-of-Elyria




## Contributing

Want to help? Great! Join at Discord [here](https://discord.gg/VtbNmgy) and let's chat! Also see roadmap below and check issues tagged "good first issue".

Technical people, please refer to [CONTRIBUTING.md](./CONTRIBUTING.md) for technical details.

## High-level roadmap

1. [x] Proof-of-concept using known D3
2. [x] Use OpenLayers
3. [ ] API for custom public maps
4. [ ] User Interface
5. [ ] Support all servers and kingdoms
6. [ ] Add new userdata: "custom domain meta"
7. [ ] Interactive userdata editing
8. [ ] Security & Private maps

