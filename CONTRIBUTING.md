# Contributing

To contribute please read on to get you started. Those are few simple steps:

1. Join Cartography App discord server (discord.gg/VtbNmgy) and say hello to everyone.
2. Introduce yourself at `#i-want-to-contribute` channel so we know who's who.
3. Find time with @Akuukis so he can onboard you. Most of the things you can do on your own:
   1. if you are **looking forward** to code, see details at [CONTRIBUTING-CODE.md](./CONTRIBUTING-CODE.md).
   2. if you are **not looking forward** to code, see details at [CONTRIBUTING-PROJECT.md](./CONTRIBUTING-PEOPLE.md).
4. Do your first little contribution and @Akuukis will swap your tag to "contributor" tag.

Congratz, done!



## Detailed roadmap in somewhat priority (updated 2019-11-5)

1. [x] Proof-of-concept using known D3
2. [x] Use OpenLayers
   1. [x] Learn OpenLayers
   2. [x] Rewrite base map
   3. [ ] Rewrite annotations
   4. [ ] Rewrite export as PNG
3. [ ] API for custom public maps (via url parameters)
   1. [x] userdata links
   2. [ ] coords
   3. [ ] extent
   4. [ ] datasets
   5. [ ] infobox controls
   6. [ ] zoom
   7. [ ] title
   8. [ ] description
   9. [ ] layers controls
   10. [ ] highlights
   11. [ ] etc.
4. [ ] User Interface
   1. [ ] select framework *(I know React but that's an overkill.. anyone?)*
   2. [ ] static frame stuff - drawers, panels, links, etc.
   3. [ ] basic controls: coords, extent, datasets, zoom, etc.
   4. [ ] infobox controls
   5. [ ] layer controls
   6. [ ] userdata controls
   7. [ ] searchbox + selected feature info
5. [ ] Support all servers and kingdoms
   1. [ ] landing page
   2. [ ] Angelica
   3. [ ] Luna
   4. [x] Selene
   5. [ ] Oceanus
6. [ ] Add new userdata: "custom domain meta"
   1. [ ] sheets structure
   2. [ ] sheets API
   3. [ ] infobox support for custom meta & extend layout-ing
7. [ ] Interactive userdata editing
   1. [ ] click-n-drop editing
   2. [ ] save back to sheet
8. [ ] Security
   1. [ ] login with Google OAuth2
   2. [ ] read protected sheets as logged-in user
   3. [ ] write to protected sheets as logged-in user
